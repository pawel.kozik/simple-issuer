const express = require("express");

const app = express();
const port = 3001;

app.use(express.json());

const log = (data) => console.log(JSON.stringify(data, null, 4));

app.post(
  "/payments",
  /**validator.validate("post", "/transfers"),**/ (req, res) => {
    log(req.headers);
    log(req.body);
    log(req.trailers);

    //console.log("Verified payload: " + getSignatureVerifyResult(req));

    res.send({
      status: "AUTHORIZED",
    });
  }
);

app.post("/account-links", (req, res) => {
  log(req.body);

  res.send({
    accountLinks: [
      {
        accountLinkId: "dac1802b-cee2-4952-bb98-5055ba464d22",
        ownerId: "c6015541-100a-41ce-98c7-ad9923734be0",
      },
    ],
    user: {
      id: "c6015541-100a-41ce-98c7-ad9923734be0",
    },
  });
});

app.get("/account-links/:accountLinkId", (req, res) => {
  log(req.body);

  res.send({
    status: "ACTIVE",
  });
});

///// V1.0

app.post("/approve-payment", (req, res) => {
  log(req.body);

  res.send({
    status: "APPROVED",
  });
});

app.post("/approve-and-settle-payment", (req, res) => {
  log(req.body);

  res.send({
    status: "APPROVED",
    settlement: {
      identifier: "test",
    },
  });
});

app.post("/reverse-payment", (req, res) => {
  log(req.body);

  res.send({
    status: "APPROVED",
  });
});

app.post("/approve-refund", (req, res) => {
  log(req.body);

  res.send({
    status: "APPROVED",
  });
});

app.post("/approve-and-settle-refund", (req, res) => {
  log(req.body);

  res.send({
    status: "APPROVED",
    settlement: {
      identifier: "test",
      targetAccount: "accountIban",
    },
  });
});

app.post("/reverse-refund", (req, res) => {
  log(req.body);

  res.send({
    status: "APPROVED",
  });
});

app.post("/approve-token-request", (req, res) => {
  log(req.body);

  res.send({
    status: "APPROVED",
  });
});

app.listen(port, () => {
  console.log(`Local issuer listening on port ${port}`);
});
